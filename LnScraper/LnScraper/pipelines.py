# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.utils.log import configure_logging
from .config import *
from .db.novelRethink import *
from .items import Chapter,BlacklistUrl
import logging


configure_logging()

class ChapterPipeline(object):


    def __init__(self): 
        self.novelRethink = lightnovelRethink()

    def process_item(self, item, spider):
        
        if not isinstance(item,Chapter):
            return item
        name = item["name"]
        novelurl = item["novelurl"]
        del item['name']
        if 'content' in item:
            self.novelRethink.updateChapters(novelurl,dict(item))
            logging.info(item['content'])
        else:
            logging.info("Content of Novel: {} Chapter:{} Volume:{} Blocked".format(name,item['chapter'],item['volume']))



class NovelPipeline(object):

    def __init__(self):
         self.novelRethink = lightnovelRethink()

    def process_item(self, item, spider):
        logging.info(item)
        novelurl = item["novelurl"]
        self.novelRethink.updateNovel(novelurl, dict(item))
        logging.info("Inserted {} in novels".format(item['name']).encode('utf-8'))
          

class BlacklistUrlPipeline(object):
    def __init__(self):
        self.novelRethink = lightnovelRethink()

    def process_item(self,item,spider):
        if not isinstance(item,BlacklistUrl):
            return item
        self.novelRethink.updateBlacklistChapterUrls(item['chapterurl'])
