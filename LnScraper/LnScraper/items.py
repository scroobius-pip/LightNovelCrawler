# -*- coding: utf-8 -*-



import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose
from scrapy import Selector
from lxml import html
from lxml.html.clean import Cleaner
import re
import base64
import requests

def li_a(g):
    sel = Selector(text=g)
    return sel.css("li a::text").extract()

def li(g):
    sel = Selector(text=g)
    return sel.css("li::text").extract()

def clean_html(g):
    clean = Cleaner(safe_attrs=html.defs.safe_attrs)
    clean.javascript = True
    clean.style = True
    clean.safe_attrs_only = True
    g = "".join(g)
    return clean.clean_html(g)


def arr_to_string(g):
    try:
        return g[0]
    except:
        return ""


def arr_to_int(g):
    try:
        x = int(g[0])
        return x
    except:
        return ""

def to_float(g):
    try:
        x = float(g)
        return x
    except:
        return g

def get_as_base64(url):
    if url:
        try:
            return base64.b64encode(requests.get(url).content)
        except:
            return ""
    else:
        return ""

class Novel(scrapy.Item):
    name = scrapy.Field()
    type = scrapy.Field()
    genre = scrapy.Field()
    tags = scrapy.Field()
    language = scrapy.Field()
    author = scrapy.Field()
    artist = scrapy.Field()
    year = scrapy.Field()
    status = scrapy.Field()
    description = scrapy.Field()
    novelurl = scrapy.Field()
    image = scrapy.Field()

class Chapter(scrapy.Item):
    name = scrapy.Field()
    volume = scrapy.Field() #volume number
    chapter = scrapy.Field() 
    content = scrapy.Field() # There are escaped characters that should be processed by the client 
    chapterurl = scrapy.Field()
    novelurl = scrapy.Field()


class BlacklistUrl(scrapy.Item):
    chapterurl = scrapy.Field()

class NovelItemLoader(ItemLoader):
    default_item_class = Novel
    name_out = Compose(lambda x: x[0])
    type_out = Compose(lambda x: x[0])
    genre_out = Compose(lambda x: x[1], li_a)
    tags_out = Compose(lambda x: x[2], li_a)
    language_out = Compose(lambda x: x[3], li_a, arr_to_string)
    author_out = Compose(lambda x: x[4], li_a, arr_to_string)
    artist_out = Compose(lambda x: x[5], li_a, arr_to_string)
    year_out = Compose(lambda x: x[6], li, arr_to_int)
    status_out = Compose(lambda x: x[7], li, arr_to_string)
    description_out = Compose(clean_html)
    novelurl_out = Compose(lambda x: str(x[0]))
    image_out = Compose(arr_to_string,get_as_base64,lambda x: str(x))

class ChapterItemLoader(ItemLoader):
    default_item_class = Chapter
    name_out = Compose(lambda x: x[0])
    volume_out = Compose(lambda x: x[0],to_float)
    content_out = Compose(clean_html,lambda x: ' '.join(x.split())) 
    chapterurl_out = Compose(lambda x: x[0])
    chapter_out = Compose(lambda x: x[0],to_float)
    novelurl_out = Compose(lambda x: str(x[0]))

class BlacklistUrlLoader(ItemLoader):
    chapterurl_out = Compose(lambda x :x[0])