##from ..config import *
import rethinkdb as r
from rethinkdb.errors import RqlRuntimeError, RqlDriverError
import logging
import uuid

connection = r.connect("api.lightnoveler.com", 28015)
novels = r.db('test').table(
    "crawlerNovels")
novels.filter(lambda x: x.keys().contains('chapters')).map(lambda x: x['chapters']).map(lambda x: x.merge({'id': r.uuid()})).run(connection)
