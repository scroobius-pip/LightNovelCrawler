from pymongo import MongoClient
from ..config import *


#class for getting data from mongo
#mongo_config is uri for mongodb instance

class lightnovelMongo:
    def __init__(self):
        # client = MongoClient(mongo_config['ip'],mongo_config['port'])
        client = MongoClient(mongo_config_url)
        db = client[mongo_config_database]
        self.novels = db[mongo_config_collection]


    def novelUrls(self):
        # Gets all novles in db removes id and converts to set using set comprehension
        s  = {x['novelurl'] for x in list(self.novels.find({},{"novelurl":1,"_id":0}))}
        return s

    def chapterUrls(self):
        # gets all chapters url in all novel and converts to set
        s = {y['chapterurl'] for x in list(self.novels.find({},{"chapters.chapterurl":1,"_id":0})) if 'chapters' in x for y in x['chapters']  }
        return s

    def updateChapters(self,novelurl,document):
        if document is None:
            pass
        else:
            self.novels.update_one({'novelurl':novelurl},{'$currentDate':{'modified':{'$type':'date'}},'$addToSet':{'chapters':document}},upsert=True)

    def updateNovel(self,novelurl,document):
        self.novels.update_one({"novelurl":novelurl},{'$set':document,'$currentDate':{'modified':{'$type':'date'}}},upsert=True)
    