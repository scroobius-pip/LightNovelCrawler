#from scrapy.utils.log import configure_logging
from ..config import *
import rethinkdb as r
from rethinkdb.errors import RqlRuntimeError, RqlDriverError
import logging
#configure_logging()


class lightnovelRethink:
    def __init__(self):
        #print(rethink_config_database,rethink_config_url,rethink_port)
        self.connection = r.connect(rethink_config_url, rethink_port)
        
        self.novels = r.db(rethink_config_database).table(
            rethink_config_collection)
        self.blacklist = r.db(rethink_config_database).table(
            rethink_config_blacklist)

    def novelUrls(self):
        novels = {novel['novelurl']
                  for novel in self.novels.pluck('novelurl').run(self.connection)}
        return novels

    def chapterUrls(self):
        chapterurl = self.novels.filter(lambda x: x.keys().contains('chapters')).pluck({'chapters': {'chapterurl'}}).map(
            lambda x: x['chapters']).map(lambda x: x['chapterurl']).filter(lambda x: x != []).run(self.connection)
        return {y for x in chapterurl for y in x}

    def updateChapters(self, novelurl, document):
        if document is None:
            pass
        else:
            
            document['id'] = r.uuid() # generates a unique id per chapter
            document['modified'] = r.now().to_iso8601()
            novelurl = novelurl[:120] #will truncate novelurl if greater than 120
            self.novels.insert({"id": novelurl}, conflict="update")
            self.novels.get(novelurl).update({"chapters": r.row["chapters"].default(
                []).append(document), "modified": r.now().to_iso8601()},non_atomic=True).run(self.connection, durability='soft')
            
                

    def updateNovel(self, novelurl, document):
        if document is None:
            pass
        else:
            document['id'] = novelurl[:120] #will truncate novelurl if greater than 120
            document['modified'] = r.now().to_iso8601()
            document['uuid'] = r.uuid()
           # print(self.novels)
            self.novels.insert(document, conflict="update").run(self.connection)

    def blacklistChapterUrls(self):
        # create chapterblacklist document with 'chapterurl' id
        # create novelblacklist document with 'novelurl' id
        chapterBlacklist = self.blacklist.get('chapterurl')['chapters'].default([]).run(self.connection)
        return set(chapterBlacklist)

    def updateBlacklistChapterUrls(self, url):
        self.blacklist.get('chapterurl').update(
            {"chapters": r.row['chapters'].default([]).append(url)}).run(self.connection)

    def blacklistNovelUrls(self):
        novelBlacklist = self.blacklist.get('novelurl')['novels'].default([]).run(self.connection)
        return set(novelBlacklist)

    def updateBlacklistNovelUrls(self, url):
        self.blacklist.get('novelurl').update(
            {"novels": r.row['novels'].default([]).append(url)}).run(self.connection)

    def idUpdater(self):
        self.novels.filter(lambda x: x.keys().contains('chapters')).map(lambda x: x['chapters']).map(lambda x: x.merge({'id': r.uuid()})).run(self.connection)