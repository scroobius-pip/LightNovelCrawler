#!c:\python27\python.exe
from LnScraper.spiders.lightnovel import chapterSpider,lightnovelSpider,initCrawler
from scrapy.crawler import CrawlerProcess,CrawlerRunner
from twisted.internet import reactor, defer
from scrapy.utils.log import configure_logging
import logging

configure_logging()

runner = CrawlerRunner()

@defer.inlineCallbacks
def crawl():
    yield runner.crawl(initCrawler)
    toNovel =    initCrawler.toNovel
    toChapter =  initCrawler.toChapter
    yield runner.crawl(lightnovelSpider,novels=toNovel)
    yield runner.crawl(chapterSpider,novels=toChapter)
    reactor.stop()

crawl()
reactor.run()
