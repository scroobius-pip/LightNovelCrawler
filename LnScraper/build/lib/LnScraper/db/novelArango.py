from scrapy.utils.log import configure_logging
from pyArango.connection import *
from ..config import *
import logging

configure_logging()

class lightnovelArango:
    def __init__(self):
        client = Connection(arangoURL=arango_config_url,
                            username="root", password="root")
        self.db = client[arango_config_database]
        #self.novels = self.db[arango_config_collection]

    def novelUrls(self):
        aql = """FOR x in novels FILTER x.novelurl != NULL RETURN  x.novelurl"""
        #bindVars = {"collection": arango_config_collection}
        queryResult = self.db.AQLQuery(
            aql, rawResults=True, batchSize=100)
        return set(queryResult)

    def chapterUrls(self):
        aql = """FOR novel in novels FILTER novel.chapters != NULL
 FOR chapter in novel.chapters RETURN chapter.chapterurl"""
        queryResult = self.db.AQLQuery(aql, rawResults=True, batchSize=100)
        return set(queryResult)

    def updateChapters(self, novelurl, document):
        if document is None:
            pass
        else:
            aql = """
            UPSERT {novelurl:@novelurl}
            INSERT {novelurl:@novelurl,modified:DATE_NOW(),chapters:PUSH(OLD.chapters,@doc)}
            UPDATE {modified:DATE_NOW(),chapters:PUSH(OLD.chapters,@doc)} IN novels
            """
            bindVars = {"novelurl": novelurl,
                        "doc": document}
            self.db.AQLQuery(aql, bindVars=bindVars)

    def updateNovel(self, novelurl, document):
        if document is None:
            pass
        else:
            aql = """ UPSERT {novelurl:@novelurl}
                      INSERT MERGE( {modified:DATE_NOW()},@doc)
                      UPDATE MERGE( {modified:DATE_NOW()},@doc) IN novels
                  """
            bindVars = {"novelurl": novelurl,
                        "doc": document}
            self.db.AQLQuery(aql, bindVars=bindVars)

    def blacklistChapterUrls(self):
        aql = """ LET doc = DOCUMENT("blacklist/chapterurl")
                  FOR x in doc.chapters
                  RETURN x
              """
        queryResult = self.db.AQLQuery(aql, rawResults=True, batchSize=100)
        return set(queryResult)

    def updateBlacklistChapterUrls(self, url):
        logging.info("url passed to updateBlacklistNovelUrls {}".format(url))

        aql = """  UPSERT {"_key":"chapterurl"}
                   INSERT {chapters:PUSH(OLD.chapters,@url)}
                   UPDATE {chapters:PUSH(OLD.chapters,@url)} IN blacklist
              """
        bindVars = {"url": url}

        self.db.AQLQuery(aql, bindVars=bindVars)

    def blacklistNovelUrls(self):
        aql = """ LET doc = DOCUMENT("blacklist/novelurl")
                  FOR x in doc.novels
                  RETURN x
              """
        queryResult = self.db.AQLQuery(aql, rawResults=True, batchSize=100)
        return set(queryResult)

    def updateBlacklistNovelUrls(self, url):
        aql = """ UPSERT {"_key":"novelurl"}
                  INSERT {novels:PUSH(OLD.novels,@url)}
                  UPDATE {novels:PUSH(OLD.novels,@url)} IN blacklist
              """
        bindVars = {"url": url}
        self.db.AQLQuery(aql, bindVars=bindVars)
