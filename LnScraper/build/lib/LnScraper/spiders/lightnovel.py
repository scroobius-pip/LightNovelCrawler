# -*- coding: utf-8 -*-
import scrapy
import logging
from scrapy import Selector
from ..items import Novel, Chapter, BlacklistUrl, NovelItemLoader, ChapterItemLoader, BlacklistUrlLoader
from ..db.novelArango import *


class lightnovelSpider(scrapy.Spider):

    name = "novelDetail"
    allowed_domains = ["readlightnovel.com"]

    custom_settings = {
        "DOWNLOAD_DELAY": 2,
        'ITEM_PIPELINES': {
            'LnScraper.pipelines.NovelPipeline': 300
        }
    }

    def __init__(self, novels=[]):
        self.novels = novels

    def start_requests(self):
        for novel in self.novels:
            self.logger.info(novel)
            request = scrapy.Request(novel, callback=self.parseNovel)
            yield request

    def parseNovel(self, response):

        l = NovelItemLoader(response=response)
        l.add_xpath('type',
                    '//div[@class="novel-details"]/div[@class="novel-detail-item"]/div[@class="novel-detail-body"]/ul/li/a/text()')
        l.add_value('novelurl', response.url)
        l.add_css('name', '.block-title h1::text')
        selector_xpath = '//div[@class="novel-details"]/div[@class="novel-detail-item"]'
        l.add_xpath('genre', selector_xpath)
        l.add_xpath('tags', selector_xpath)
        l.add_xpath('language', selector_xpath)
        l.add_xpath('author', selector_xpath)
        l.add_xpath('artist', selector_xpath)
        l.add_xpath('year', selector_xpath)
        l.add_xpath('status', selector_xpath)
        l.add_xpath('description',
                    '//div[@class="novel-right"]/div[@class="novel-details"]/div[@class="novel-detail-item"]/div[@class="novel-detail-body"]/p')
        l.add_xpath(
            'image', '//div[@class="novel-cover"]/a/img[not(contains(@src,"noimage"))]/@src')
        return l.load_item()


class chapterSpider(scrapy.Spider):
    name = "chapters"
    allowed_domains = ["readlightnovel.com"]
    custom_settings = {
        "DOWNLOAD_DELAY": 2.5,
        # "FEED_EXPORT_ENCODING":"utf-8",
        # "FEED_FORMAT":"json",
        # "FEED_URI":"chapters.json",
        'ITEM_PIPELINES': {
            'LnScraper.pipelines.ChapterPipeline': 300,
            'LnScraper.pipelines.BlacklistUrlPipeline': 500

        }

    }

    def __init__(self, novels=[]):
        self.logger.info("Novels passed as parameters:{}".format(novels))
        self.novels = novels
        novelArango = lightnovelArango()
        self.ArangoChapters = novelArango.chapterUrls()
        self.BlacklistChapters = novelArango.blacklistChapterUrls()

    def isNumber(self, g):
        try:
            float(g)
            return True
        except:
            return False

    def extractor(self, url):
        url_split = url
        try:
            CH = url_split.index("CH")
            VOLUME = url_split.index("Volume")
        except:
            return None

        try:
            CH_VALUE = url_split[CH + 1]
            VOLUME_VALUE = url_split[VOLUME + 1]
        except:
            print("Invalid")
            return None

        if self.isNumber(CH_VALUE) and self.isNumber(VOLUME_VALUE):
            print("Index CH:{} Index Volume:{} || Value CH:{} ,Value Volume:{} Title {}".format(
                CH, VOLUME, CH_VALUE, VOLUME_VALUE, url))
            return [float(CH_VALUE), float(VOLUME_VALUE)]
        else:
            return None

    def start_requests(self):
        self.logger.info("Novel Count: {}".format(len(self.novels)))
        for novel in self.novels:
            self.logger.info(novel)
            request = scrapy.Request(novel, callback=self.parseChapters)
            yield request

    def parseChapters(self, response):
        chapters = {}

        name = response.css(".block-title h1::text").extract_first()
        chapters = response.xpath(
            '//div[@class="tab-content"]/div/ul/li/a/@href').extract()
        noChaptersFound = len(chapters)
        # remove invalid chapters that end with -
        chapters = {x for x in chapters if not x.endswith("-")}
        chapters -= self.ArangoChapters
        chapters -= self.BlacklistChapters

        url = response.url
        self.logger.info("Name :{} | No Chapters in Arango {} | No Chapters Found Online {} | No Chapters to download {} ".format(
            name, len(self.ArangoChapters), noChaptersFound, len(chapters)))

        if not chapters:
            self.logger.info("Skipping")
        else:
            for chapter in chapters:

                request = scrapy.Request(chapter, callback=self.getChapters)
                request.meta["name"] = name
                request.meta["novelurl"] = url
                yield request

    def getChapters(self, response):

        l = ChapterItemLoader(item=Chapter(),response=response)
        b = BlacklistUrlLoader(item=BlacklistUrl(),response=response)
        name = response.meta["name"]
        novelurl = response.meta["novelurl"]
        details = response.xpath(
            '//select/option[@selected="selected"]/text()').extract_first()
        chapterurl = response.url
        details = details.split(" ")

        # self.logger.info("Name: {}".format(name))
        # self.logger.info("Details:")
        # self.logger.info(content)

        if (len(details) == 4):
            volume = details[1]
            chapter = details[3]

        elif (len(details) == 5):
            volume = details[1]
            chapter = details[4]
        else:
            self.logger.info(
                "Unexpected length of chapter details: Novel: {} ChapterUrl: {}".format(name, chapterurl))
            details_extractor = self.extractor(url=details)
            if details_extractor is None:
                self.logger.info("Could Not Parse {}".format(chapterurl))
                b.add_value('chapterurl', chapterurl)
                return b.load_item()
            else:
                chapter = details_extractor[0]
                volume = details_extractor[1]
                self.logger.info("Parsed Succesfully {} ChapterUrl {}".format(
                    details_extractor, chapterurl))

        l.add_value('name', name)
        l.add_value('novelurl', novelurl)
        l.add_value('chapterurl', chapterurl)
        l.add_xpath(
            'content', '//div[@class="chapter-content3"]/node()[ not(self::noscript or self::center or @class="row")]')
        l.add_value('chapter', chapter)
        l.add_value('volume', volume)
        return l.load_item()


class initCrawler(scrapy.Spider):
    name = "main"
    # fromMongo = [] # will get all novel urls in mongo
    toChapter = []
    toNovel = []
    fromScraper = set()

    def start_requests(self):
        urls = ['http://www.readlightnovel.com/novel-list']

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):

        for novel in response.xpath('//div[@class="list-by-word-body"]/ul/li/a/@href[not(@href="#")]').extract():
            # initCrawler.fromScraper.append(novel)
            initCrawler.fromScraper.add(novel)

        self.checkchanged()

    def checkchanged(self):
        novelArango = lightnovelArango()
        novelUrlBlacklist = novelArango.blacklistNovelUrls()
        novelUrl = novelArango.novelUrls()
        #initCrawler.fromScraper -= novelUrlBlacklist
        initCrawler.toChapter = set(initCrawler.fromScraper)
        initCrawler.toChapter -= novelUrlBlacklist

        #initCrawler.fromScraper -= novelUrl
        initCrawler.toNovel = set(initCrawler.fromScraper)
        initCrawler.toNovel -= novelUrl
        initCrawler.toChapter -= novelUrlBlacklist
        self.logger.info('Filtered fromScraper:{} From novelurl  {} from blacklisturl {} result: {}'.format(
            len(initCrawler.fromScraper), len(novelUrl), len(novelUrlBlacklist), len(initCrawler.toChapter)))
